from flask import current_app, request, render_template, render_template_string
from random import randint
from re import findall
from jobs import admin
from urllib.parse import urlparse

def validator(link):
    result = urlparse(link)
    print(result)
    if not result.netloc:
        return "not url"
    if result.netloc != current_app.config.get('CURRENT_URL'):
        return "not valid"

def index():
    if request.method == 'POST':
        text = request.form.get('text')
        if not text:
            return render_template('index.html', message = "Empty field.")
        if "document" in text:
            return render_template('index.html', 
                                        message = "Are you hacker?")
        length = len(text)
        longest = len(max(findall(r'\S+',text), key = len))
        message = f"You data: {text}; Length: {length}; Longest word {longest}"
        filename = randint(100000,10000000)
        open(f"files/{filename}.txt", 'w').write(message)
        file_link = f"http://{current_app.config.get('CURRENT_URL')}/files/{filename}.txt"
        return render_template('index.html', 
                                message = message, link=file_link)
    return render_template('index.html')

def download(filename):
    return render_template_string(open(f"files/{filename}").read())

def report():
    if request.method == "POST":
        link = request.form.get("link")
        if validator(link) == "not url":
            return render_template("report.html", message="Please, send me link!")
        if validator(link) == "not valid":
            return render_template("report.html", message="Are you hacker? Founded link to another site.")
        admin.queue(link)
        return render_template("report.html", message="Admin will check your file soon...")
    return render_template("report.html")