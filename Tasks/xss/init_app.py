import os
from dotenv import load_dotenv
from flask import Flask
from logic import index, report, download

def create_app():
    app = Flask(__name__)
    load_dotenv()
    app.config['CURRENT_URL'] =  f"{os.environ['CURRENT_URL']}"

    from jobs import rq
    rq.init_app(app)

    app.add_url_rule('/', 'index', index, methods=["GET", "POST"])
    app.add_url_rule('/report', 'report', report, methods=["GET", "POST"])
    app.add_url_rule('/files/<path:filename>', '/files/<path:filename>', download)
    return app