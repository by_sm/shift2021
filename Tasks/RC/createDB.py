import sqlite3

conn = sqlite3.connect("database.db")
c = conn.cursor()

c.execute("""CREATE TABLE IF NOT EXISTS 
users(id INTEGER PRIMARY KEY, login TEXT, password TEXT, balance INT, uuid TEXT, act INT)""")
conn.commit()
c.execute("""CREATE TABLE IF NOT EXISTS 
prod(id INTEGER PRIMARY KEY, product TEXT, description TEXT, price INT)""")
conn.commit()
c.execute("""INSERT INTO prod(product, description, price) VALUES (?, ?, ?)""", ("Phone", "88005553535", 40))
conn.commit()
c.execute("""INSERT INTO prod(product, description, price) VALUES (?, ?, ?)""", ("testFlag", "CTF{test}", 50))
conn.commit()
c.execute("""INSERT INTO prod(product, description, price) VALUES (?, ?, ?)""", ("Dollar", "$", 30))
conn.commit()
c.execute("""INSERT INTO prod(product, description, price) VALUES (?, ?, ?)""", ("Ruble", "₽", 10))
conn.commit()
c.execute("""INSERT INTO prod(product, description, price) VALUES (?, ?, ?)""", ("Flag", "CTF{ju57_83_f45732_7h4n_84ck3nd_532v32}", 500))
conn.commit()
conn.close()