from flask import Flask, render_template, request, g, redirect, make_response, render_template_string, session, send_from_directory
import sqlite3
from time import sleep
from uuid import uuid4
from hashlib import sha256
from flask.wrappers import Response

app = Flask(__name__)
app.database = "database.db"
app.secret_key = "QSKMALKMKMASDOIWDJ898*&SD*A&H"

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

def connect_db():
    return sqlite3.connect(app.database)

@app.route('/', methods=['GET'])
def index_raw():
    if not session:
        return make_response(redirect('/login'))
    else:
        return make_response(redirect('/index'))

@app.route('/exit', methods=['GET', 'POST'])
def exit():
    session.clear()
    return make_response(redirect('/'))

@app.route('/index', methods=["GET"])
def index():
    try:
        uuid = session['token']
    except:
        return make_response(redirect('/login'))
    g.db = connect_db()
    user = g.db.execute("SELECT login FROM users WHERE uuid=?", (uuid)).fetchone()[0]
    balance = g.db.execute("SELECT balance FROM users WHERE uuid=?", (uuid)).fetchone()[0]
    if user:
            return render_template("index.html", balance=balance)
    else:
        return make_response(redirect('/login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        login = request.form.get("login")
        password = sha256(request.form.get("password").encode()).hexdigest()
        g.db = connect_db()
        uuid = g.db.execute("""SELECT uuid FROM users WHERE login = ? AND password = ?""", (login, password)).fetchone()
        if uuid:
            session['token'] = uuid 
            return make_response(redirect('/index'))
        else:
            return render_template('login.html', error="Error. Try again.")
    return render_template('login.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == "POST":
        login = request.form.get('login')
        password = sha256(request.form.get("password").encode()).hexdigest()
        uuid = str(uuid4())
        g.db = connect_db()
        unique_login = g.db.execute("SELECT COUNT(login) FROM users WHERE login=?", (login,)).fetchone()[0]
        if unique_login != 0:
            g.db.close()
            return render_template('register.html', error="User already exists.")
        else:
            g.db.execute("INSERT INTO users(login,password,uuid,balance,act) VALUES(?,?,?,?,?)", (login, password, uuid, 0, 0))
            g.db.commit()
            g.db.close()
            return (make_response(redirect('/login')))
    return render_template('register.html')

@app.route('/free', methods=["GET", "POST"])
def free():
    try:
        uuid = session['token']
    except:
        return make_response(redirect('/login'))
    if request.method == "POST":
        db = connect_db()
        act = db.execute("SELECT act FROM users WHERE uuid=?", (uuid[0],)).fetchone()[0]
        if act == 1:
            return render_template("free.html", response="You have already received 50 free coins!")
        else:
            sleep(1)
            db.execute("UPDATE users SET balance=balance+? WHERE uuid=?", (50, uuid[0]))
            db.execute("UPDATE users SET act=1 WHERE uuid=?", (uuid[0],))
            db.commit()
            db.close()
            return render_template("free.html", response="Congratulations, you've got 50 Free coins!")
    return render_template("free.html")

@app.route('/shop', methods=["GET", "POST"])
def shop():
    try:
        uuid = session['token']
    except:
        return make_response(redirect('/login'))
    g.db = connect_db()
    balance = g.db.execute("SELECT balance FROM users WHERE uuid=?", (str(uuid[0]),)).fetchone()[0]
    if request.method == "POST":
        product = request.form.get('product')
        product_price = g.db.execute("SELECT price FROM prod WHERE product=?", (str(product),)).fetchone()[0]
        if balance >= product_price:
            new_balance = balance-product_price
            g.db.execute("UPDATE users SET balance=? WHERE uuid=?", (new_balance, str(uuid[0])))
            g.db.commit()
            desc = g.db.execute("SELECT description FROM prod WHERE product=?", (str(product),)).fetchone()[0]
            balance = g.db.execute("SELECT balance FROM users WHERE uuid=?", (str(uuid[0]),)).fetchone()[0]
            g.db.close()
            return render_template("shop.html", response=f"Item purchased successfully! You item: {desc}", balance=balance)
        else:
            return render_template("shop.html", response="You do not have enough coins!", balance=balance)
    return render_template("shop.html", balance=balance)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8090, threaded=True)