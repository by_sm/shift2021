<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Empty site</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
            html, body {
                background-color: #000;
                color: #fff;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Catch Zen, be calm.
                    <!-- Frank, don't forget to delete all backup (*.bak) files before releasing the site to production! -->
                </div>
            </div>
        </div>
    </body>
</html>
<?php
    $db = new SQLite3('sqlitedb', SQLITE3_OPEN_READWRITE);
    $data = $_GET['query'];
    $result = $db->querySingle($data);
?>

